from setuptools import setup


setup(
    author='Christophe Pettus',
    author_email='xof@thebuild.com',
    name='xact',
    version='1.0',
    py_modules=['xact'],
)
